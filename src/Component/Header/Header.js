import React from "react";
import icon_header from "../../img/logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../Service/localService";

export default function Header() {
  let textHover = " text-black hover:text-red-500  ";
  // lấy dữ liệu từ redux
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogOut = () => {
    localServ.remove();
    window.location.href = "/";
  };
  let renderWithHeader = () => {
    if (userInfo) {
      return (
        <div className="flex justify-end items-center">
          <span className="bg-transparent hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4    rounded mr-1 ">
            {userInfo.hoTen}
          </span>
          <button
            className="bg-transparent hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4    rounded mr-1 "
            onClick={handleLogOut}
          >
            Đăng Xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex justify-end items-center">
          <NavLink to="/login">
            <button class="bg-transparent hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4    rounded mr-1 ">
              <FontAwesomeIcon
                icon={faUserCircle}
                className="mr-5 text-slate-500"
                size="lg"
              />
              Đăng Nhập
            </button>
          </NavLink>

          <button class="bg-transparent rounded hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4   ">
            <FontAwesomeIcon
              icon={faUserCircle}
              className="mr-5 text-slate-500"
            />
            Đăng ký
          </button>
        </div>
      );
    }
  };
  return (
    <div className="flex justify-between items-center container mt-10 ">
      <div className="flex justify-between items-center  ">
        <img
          alt="#"
          src={icon_header}
          style={{
            width: 250,
            height: 100,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
          }}
        />
        <ul
          className="list-none font-medium text-xl flex justify-between items-center ml-20 "
          style={{ width: 500 }}
        >
          <li className={textHover}>Lịch Chiếu</li>
          <li className={textHover}>Cụm Rạp</li>
          <li className={textHover}>Tin Tức</li>
          <li className={textHover}>Ứng Dụng</li>
        </ul>
      </div>
      <div>{renderWithHeader()}</div>
    </div>
  );
}
