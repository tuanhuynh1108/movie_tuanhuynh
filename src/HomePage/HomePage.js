import React from "react";
import Layout from "../Layout/Layout";
import Header from "../Component/Header/Header";
import CarouselBanner from "./Carousel/CarouselBanner";

export default function HomePage() {
  return (
    <div>
      <CarouselBanner />
    </div>
  );
}
