import React, { useEffect, useState } from "react";

import { movieServ } from "../../Service/movieBanner";
import ItemBanner from "./ItemBanner";

export default function CarouselBanner() {
  //   const onChange = (currentSlide) => {
  //     console.log(currentSlide);
  //   };
  const [bannerMovie, setBannerMovie] = useState([]);
  useEffect(() => {
    movieServ
      .banner()
      .then((res) => {
        console.log(res);
        setBannerMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="w-full h-full border-red-600 ">
      <ItemBanner bannerMovie={bannerMovie} />
    </div>
  );
}
