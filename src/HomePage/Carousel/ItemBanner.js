import React from "react";
import { Carousel } from "antd";
const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
export default function ItemBanner({ bannerMovie }) {
  let renderBanner = () => {
    return (
      <Carousel autoplay>
        {bannerMovie.map((image) => {
          return (
            <div key={image.id} className="w-screen h-screen ">
              <img
                src={image.hinhAnh}
                alt="#"
                style={{
                  objectPosition: "top",
                  objectFit: "cover",
                }}
              />
              ;
            </div>
          );
          console.log("item.hinhAnh: ", image.hinhAnh);
        })}
        {/* {bannerMovie.map((image) => (
          <div key={image.id}>
            <img src={image.hinhAnh} alt="#" />
          </div>
        ))} */}
      </Carousel>
    );
  };
  return <div>{renderBanner()}</div>;
}
