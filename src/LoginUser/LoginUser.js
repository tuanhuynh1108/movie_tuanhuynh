import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userServ } from "../Service/userServ";
import { useNavigate } from "react-router-dom";
import { localServ } from "../Service/localService";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../redux/constant/userConstant";
export default function LoginUser() {
  const navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        console.log(res.data.content);
        message.success("đăng nhập thành công");
        // đăng nhâp thành công đưa về HomePage
        navigate("/");
        // lưu xuống local
        localServ.set(res.data.content);
        // đẩy dữ liệu lên redux
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
        message.success("đăng nhập thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className=" h-screen w-screen bg-slate-700 flex items-center justify-center ">
      <div className="bg-slate-50 w-11/12 rounded p-10 ">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
