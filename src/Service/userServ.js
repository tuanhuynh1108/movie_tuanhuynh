import axios from "axios";
import { BASE_URl, configHeader } from "../config/config";

export const userServ = {
  postLogin: (formLogin) => {
    return axios({
      url: `${BASE_URl}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: formLogin,
      headers: configHeader(),
    });
  },
};
