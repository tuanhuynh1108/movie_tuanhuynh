import axios from "axios";
import { BASE_URl, configHeader } from "../config/config";

export const movieServ = {
  banner: () => {
    return axios.get(`${BASE_URl}/api/QuanLyPhim/LayDanhSachBanner`, {
      headers: configHeader(),
    });
  },
};
