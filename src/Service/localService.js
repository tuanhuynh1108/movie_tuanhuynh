export const localServ = {
  get: () => {
    let dataJson = localStorage.getItem("USER_LOGIN");
    return JSON.parse(dataJson);
  },
  set: (userInfo) => {
    let dataJson = JSON.stringify(userInfo);
    localStorage.setItem("USER_LOGIN", dataJson);
  },
  remove: () => {
    localStorage.removeItem("USER_LOGIN");
  },
};
