import logo from "./logo.svg";
import "./App.css";
import Header from "./Component/Header/Header";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./HomePage/HomePage";
import LoginUser from "./LoginUser/LoginUser";
import Layout from "./Layout/Layout";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<LoginUser />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
